package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class InputFromKeyboard {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("What's your name: ");
		String name = keyboard.nextLine();
		
		System.out.print("How old are you: ");
		int age = keyboard.nextInt();
		
		System.out.print("What's your height: ");
		double height = keyboard.nextDouble();
		
		System.out.println("Mr./Mrs. " + name + " is " + age + " years old.");
		System.out.println("Your height is " + height + ".");
		
		keyboard.close();
	}

}
