package hust.soict.ictglobal.garbage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GarbageCreator {
	
	private File source;
	
	public GarbageCreator(String filename) {
		this.source = new File(filename);
	}
	
	public void execute() {
		Scanner reader;
		try {
			reader = new Scanner(this.source);
			String str = "";
			long start = System.currentTimeMillis();
			
			while (reader.hasNextLine()) {
				str += reader.nextLine();
				System.out.println(str);
			}
			
			System.out.println(System.currentTimeMillis() - start);
			reader.close();
			
		} catch (FileNotFoundException e) {
			System.out.println(e.toString());
		}
	}
	
	public static void main(String[] args) {
		GarbageCreator demo = new GarbageCreator("D:\\OOLT.HEDSPI.20192.20176745.HadoManh\\OtherProjects\\src\\hust\\soict\\ictglobal\\garbage\\something.txt");
		demo.execute();
	}

}
