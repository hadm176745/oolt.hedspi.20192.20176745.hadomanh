package hust.soict.ictglobal.aims.order;

import java.util.ArrayList;
import java.util.List;

import hust.soict.ictglobal.aims.date.MyDate;
import hust.soict.ictglobal.aims.media.Media;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 5;

	private List<Media> itemsOrdered = new ArrayList<Media>();

	private MyDate dateOrder;

	private static final int MAX_LIMITTED_ORDERS = 3;

	private static int nbOrders = 0;

	public Order() throws Exception {
		if (nbOrders < MAX_LIMITTED_ORDERS) {
			nbOrders++;
		} else
			throw new Exception("The number of orders exceeds the limit!");

		this.dateOrder = new MyDate();
	}
	
	public void addMedia(Media toAdd) {
		if (this.itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
			this.itemsOrdered.add(toAdd);
			System.out.println(toAdd.getTitle() + " has been added!");
			return;
		}
		System.out.print("The order is almost full: ");
		System.out.println(toAdd.getTitle() + " has not been added!");
	}
	
	public void removeMedia(Media toRemove) {
		
		if (itemsOrdered.remove(toRemove)) {
			System.out.println(toRemove.getTitle() + " has been removed!");
		} else {
			System.out.println(toRemove.getTitle() + " has not been ordered yet!");
		}
		
		return;
		
	}
	
	public boolean removeMediaById(int id) {
		
		for (Media media : itemsOrdered) {
			if (media.getId() ==  id) {
				itemsOrdered.remove(media);
				return true;
			}
		}
		
		return false;
		
	}

	public MyDate getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(MyDate dateOrder) {
		this.dateOrder = dateOrder;
	}

	public void getItemsOrdered() {
		this.itemsOrdered.forEach(System.out::println);
	}

	public float totalCost() {
		float total = 0;

		for (Media item : itemsOrdered) {
			total += item.getCost();
		}

		return total;
	}

	public void print() {
		System.out.println("***");
		System.out.println("Date: " + this.dateOrder);
		System.out.println("Ordered Items:");

		getItemsOrdered();

		System.out.println("Total: " + totalCost());
	}

}
