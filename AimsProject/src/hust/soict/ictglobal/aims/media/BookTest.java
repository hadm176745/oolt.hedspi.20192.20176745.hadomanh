package hust.soict.ictglobal.aims.media;

import java.util.Arrays;

public class BookTest {

	public static void main(String[] args) {
		Book demo = new Book(1, "aaa", "xxx", 102f, Arrays.asList("hadm"));
		
		demo.setContent("Hello! this is a line. It can't be hard to split into \"words\", can it?");
		
		System.out.println(demo);
	}

}
