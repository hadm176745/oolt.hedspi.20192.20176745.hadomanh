package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Book extends Media implements Comparable<Book> {
	
	private List<String> authors = new ArrayList<String>();
	
	private String content;
	
	private List<String> contentTokens;
	
	private Map<String, Integer> wordFrequency = new HashMap<String, Integer>();
	
	public Book() {
		// TODO Auto-generated constructor stub
	}
	
	public Book(int id, String title, String category, float cost, List<String> authors) {
		super(id, title, category, cost);
		this.authors = authors;
	}

	public void addAuthor(String authorName) {
		this.authors.forEach(author -> {
			if (author.equals(authorName)) {
				System.out.println("The author " + authorName + " is present in the list!");
				return;
			}
		});
		this.authors.add(authorName);
		return;
		
	}
	
	public void removeAuthor(String authorName) {
		this.authors.forEach(author ->{
			if (author.equals(authorName)) {
				this.authors.remove(authorName);
				return;
			}
		});
		System.out.println("The author " + authorName + " is not already in the list!");
		return;
		
	}
	
	private void processContent() {
		String regex = "\\W+";
		String[] results = this.getContent().toLowerCase().split(regex);
		
		this.setContentTokens(Arrays.asList(results));
		
		Collections.sort(contentTokens);
		
		this.getContentTokens().forEach(token -> wordFrequency.put(token, Collections.frequency(contentTokens, token)));
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		this.processContent();
	}

	public List<String> getContentTokens() {
		return contentTokens;
	}

	public void setContentTokens(List<String> contentTokens) {
		this.contentTokens = contentTokens;
	}

	public Map<String, Integer> getWordFrequency() {
		return wordFrequency;
	}

	public void setWordFrequency(Map<String, Integer> wordFrequency) {
		this.wordFrequency = wordFrequency;
	}

	@Override
	public String toString() {
		return "Book [authors=" + authors + ", wordFrequency=" + wordFrequency + "]";
	}

	@Override
	public int compareTo(Book toCompare) {
		return this.getTitle().compareTo(toCompare.getTitle());
	}
	
	

}
