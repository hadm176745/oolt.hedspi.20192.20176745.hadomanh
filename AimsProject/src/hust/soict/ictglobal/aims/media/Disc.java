package hust.soict.ictglobal.aims.media;

public class Disc extends Media implements Playable {
	
	private int length;
	
	private String director;
	
	public Disc() {
		// TODO Auto-generated constructor stub
	}
	
	public Disc(int id, String title, String category, String director, int length, float cost) {
		// TODO Auto-generated constructor stub
		super(id, title, category, cost);
		this.length = length;
		this.director = director;
	}
	
	public Disc(int id, String title, String category, String director, float cost) {
		// TODO Auto-generated constructor stub
		super(id, title, category, cost);
		this.director = director;
	}

	public Disc(int length, String director) {
		super();
		this.length = length;
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		
	}
	
}
