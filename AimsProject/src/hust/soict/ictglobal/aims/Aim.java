package hust.soict.ictglobal.aims;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Disc;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.order.Order;

public class Aim {
	
	private static Scanner keyboard = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Thread thread = new Thread(new MemoryDaemon());
		
		thread.setDaemon(true);
		thread.start();
		
		try {
			Order demo = null;
			int choice = 0;

			do {
				showMenu();
				System.out.print("Select: ");
				choice = keyboard.nextInt();
				keyboard.nextLine();

				switch (choice) {
				case 1:
					demo = new Order();
					System.out.println("New order created!");
					break;

				case 2:
					if (demo == null) {
						demo = new Order();
						System.out.println("New order created!");
					}
					
					System.out.println("1. Book");
					System.out.println("2. Digital Video Disc");
					System.out.println("3. Compact Disc");
					System.out.print("Choose the type: ");
					choice = keyboard.nextInt();
					keyboard.nextLine();
					
					switch (choice) {
					case 1: {
						demo.addMedia(new Book(2, "Lion King", "Animation", 19.95f, List.of("Roger Allers")));
						break;
					}
					
					case 2: {
						DigitalVideoDisc toAdd = new DigitalVideoDisc(1, "Star World", "Science Fiction", "George Lucas", 124, 24.95f);
						demo.addMedia(toAdd);
						playDisc(toAdd);
						
						toAdd = new DigitalVideoDisc(3, "Avatar", "Science Fiction", "James Cameron", 162, 29.95f);
						demo.addMedia(toAdd);
						playDisc(toAdd);
						
						break;
					}
					
					case 3: {
						List<Track> tracks = new ArrayList<Track>();
						tracks.add(new Track("111", 1));
						tracks.add(new Track("222", 2));
						tracks.add(new Track("333", 3));
						
						CompactDisc toAdd = new CompactDisc(4, "Whiplash", "Drama", "Damien Chazelle", 15.95f, "Micheal Bay", tracks);
						demo.addMedia(toAdd);
						playDisc(toAdd);
						break;
					}
					default:
						throw new IllegalArgumentException("Unexpected value: " + choice);
					}

					break;

				case 3:
					System.out.print("Enter item id: ");
					int toRemoveId = keyboard.nextInt();

					if (demo.removeMediaById(toRemoveId)) {
						System.out.println("Removed id - " + toRemoveId);
					} else {
						System.out.println("Id not found - " + toRemoveId);
					}
					break;

				case 4:
					demo.print();
					break;

				case 0:
					break;

				default:
					throw new IllegalArgumentException("Unexpected value: " + choice);
				}

			} while (choice != 0);

			keyboard.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");

	}
	
	public static void playDisc(Disc toPlay) {
		int choice;
		
		System.out.print("Play " + toPlay.getTitle() + "? 1.yes 2.no : ");
		choice = keyboard.nextInt();
		keyboard.nextLine();
		
		if (choice == 1) {
			toPlay.play();
		}
		
	}

}
