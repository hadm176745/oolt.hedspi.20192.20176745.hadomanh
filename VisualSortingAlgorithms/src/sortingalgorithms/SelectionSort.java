package sortingalgorithms;

import java.util.ArrayList;
import java.util.Arrays;

import element.Element;
import javafx.animation.ParallelTransition;
import javafx.animation.Transition;
import javafx.scene.paint.Color;
import view.AnimationController;

public class SelectionSort extends AbstractSort {

  private static final Color MININDX_COLOR = Color.ORANGE;
  private static final Color NEW_MININDX_COLOR = Color.LIGHTGREEN;

  private ParallelTransition colorElement(Element[] arr, int a, int b, Color colorA, Color colorB) {
    ParallelTransition pt = new ParallelTransition();

    pt.getChildren().addAll(colorElement(arr, colorA, a), colorElement(arr, colorB, b));

    return pt;
  }

  @Override
  public ArrayList<Transition> startSort(Element[] arr) {
	  
	  int n = AnimationController.NO_OF_ELEMENT;
	  
    ArrayList<Transition> transitions = new ArrayList<>();
    int minIndx;

    for (int i = 0; i < n - 1; i++) {
      minIndx = i;
      transitions.add(colorElement(arr, NEW_MININDX_COLOR, minIndx));

      for (int j = i + 1; j < n; j++) {
        transitions.add(colorElement(arr, SELECT_COLOR, j));
        if (arr[j].getValue() < arr[minIndx].getValue()) {
          if (minIndx == i) {
            transitions.add(colorElement(arr, minIndx, j, MININDX_COLOR, NEW_MININDX_COLOR));
          } else {
            transitions.add(colorElement(arr, minIndx, j, Color.CRIMSON, NEW_MININDX_COLOR));
          }
          minIndx = j;
        } else {
          transitions.add(colorElement(arr, START_COLOR, j));
        }
      }

      if (minIndx != i) {
        transitions.add(swap(arr, i, minIndx));
      }

        transitions.add(colorElement(arr, START_COLOR, i, minIndx));
    }

    transitions.add(colorElement(Arrays.asList(arr), SORTED_COLOR));

    return transitions;
  }
}
