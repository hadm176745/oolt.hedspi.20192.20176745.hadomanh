package sortingalgorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import element.Element;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Transition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import view.AnimationController;

public class BucketSort extends AbstractSort {

	private ArrayList<Transition> transitions;
	
	private final int BUCKET_CAPACITY = 3;

	public BucketSort() {
		this.transitions = new ArrayList<Transition>();
	}

	private List<Color> generateBucketColors(int length) {
		List<Color> bucketColors = new ArrayList<Color>();
		Random rand = new Random();
		double r, g, b;

		for (int i = 0; i < length; i++) {
			r = rand.nextDouble();
			g = rand.nextDouble();
			b = rand.nextDouble();
			bucketColors.add(new Color(r, g, b, 1));
		}

		return bucketColors;
	}

	public void bucketSort(Element[] arr) {

		int n = AnimationController.NO_OF_ELEMENT;

		// Verify input
		if (arr == null || n == 0)
			return;
		// Find the maximum and minimum values in the array
		int maxValue = arr[0].getValue(); // start with first element
		int minValue = arr[0].getValue();
		// Note: start from index 1
		for (int i = 1; i < n; i++) {
			if (arr[i].getValue() > maxValue)
				maxValue = arr[i].getValue();
			if (arr[i].getValue() < minValue)
				minValue = arr[i].getValue();
		}
		
		int m = (maxValue - minValue + 1) / BUCKET_CAPACITY + ((maxValue - minValue + 1) % BUCKET_CAPACITY == 0 ? 0 : 1);

		List<Color> bucketColors = generateBucketColors(m);

		@SuppressWarnings("unchecked")
		ArrayList<Element>[] bucket = new ArrayList[m];

		// Create empty buckets
		for (int i = 0; i < bucket.length; i++) {
			bucket[i] = new ArrayList<Element>();
		}

		// Add elements into the buckets
		for (int i = 0; i < n; i++) {
			int bucketIndex = (arr[i].getValue() - minValue + 1) / BUCKET_CAPACITY + ((arr[i].getValue() - minValue + 1) % BUCKET_CAPACITY == 0 ? -1 : 0);
			bucket[bucketIndex].add(arr[i]);
			transitions.add(colorElement(arr, bucketColors.get(bucketIndex), i));
			
			bucketIndex = (arr[i+n].getValue() - minValue + 1) / BUCKET_CAPACITY + ((arr[i+n].getValue() - minValue + 1) % BUCKET_CAPACITY == 0 ? -1 : 0);
			transitions.add(colorElement(arr, bucketColors.get(bucketIndex), i+n));
			
		}

		SequentialTransition st = new SequentialTransition();

		// Get the sorted array
		int index = 0;
		for (int i = 0; i < bucket.length; i++) {

			// Sort the elements of each bucket
			Collections.sort((bucket[i]));

			for (int j = 0, size = bucket[i].size(); j < size; j++) {

				arr[index] = bucket[i].get(j);

				FadeTransition ft = new FadeTransition(Duration.millis(100), arr[index]);
				ft.setFromValue(1.0);
				ft.setToValue(0.0);
				st.getChildren().add(ft);

				index++;

			}
		}
		
		for (int i = 0; i < n; i++) {
			st.getChildren().add(swap(arr, i, i+40));
		}

		this.transitions.add(st);

	}

	@Override
	public ArrayList<Transition> startSort(Element[] arr) {
		bucketSort(arr);

		this.transitions.add(colorElement(Arrays.asList(arr), SORTED_COLOR));

		return this.transitions;
	}

	@Override
	ParallelTransition swap(Element[] arr, int i, int j) {
		ParallelTransition pt = new ParallelTransition();
		pt.setDelay(Duration.millis(100));

		int dxFactor = j - i;

		pt.getChildren().addAll(arr[i].moveX(DX * dxFactor), arr[j].moveX(-DX * dxFactor));

		return pt;
	}

}
