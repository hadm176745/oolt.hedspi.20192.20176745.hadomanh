package sortingalgorithms;

import java.util.ArrayList;
import java.util.Arrays;

import element.Element;
import javafx.animation.Transition;
import javafx.scene.paint.Color;
import view.AnimationController;

public class HeapSort extends AbstractSort {

	private static final Color ROOT_COLOR = Color.GOLD;
	private ArrayList<Transition> transitions;

	public HeapSort() {
		this.transitions = new ArrayList<>();
	}

	private void heapify(Element[] arr, int i, int n) {
		int left = 2 * i + 1;
		int right = 2 * i + 2;
		int max = i;

		if (left < n && arr[max].getValue() < arr[left].getValue()) {
			max = left;
		}

		if (right < n && arr[max].getValue() < arr[right].getValue()) {
			max = right;
		}

		if (max != i) {
			transitions.add(swap(arr, i, max));
			heapify(arr, max, n);
		}

	}

	private void heapSort(Element[] arr) {
		
		int n = AnimationController.NO_OF_ELEMENT;

		// build initial max heap
		transitions.add(colorElement(selectSubTree(arr, n), SELECT_COLOR));
		for (int i = n / 2 - 1; i >= 0; i--) {
			heapify(arr, i, n);
		}
		transitions.add(colorElement(selectSubTree(arr, n), START_COLOR));

		// swap root node with final elt, heapify subarray
		for (int i = n - 1; i > 0; i--) {
			transitions.add(colorElement(arr, ROOT_COLOR, 0));
			transitions.add(swap(arr, 0, i));
			transitions.add(colorElement(arr, START_COLOR, i));
			transitions.add(colorElement(selectSubTree(arr, i), SELECT_COLOR));
			heapify(arr, 0, i);
			transitions.add(colorElement(selectSubTree(arr, i), START_COLOR));
		}
	}

	private ArrayList<Element> selectSubTree(Element[] arr, int n) {
		ArrayList<Element> list = new ArrayList<>();

		for (int i = 0; i < n; i++) {
			list.add(arr[i]);
		}

		return list;
	}

	@Override
	public ArrayList<Transition> startSort(Element[] arr) {
		heapSort(arr);

		transitions.add(colorElement(Arrays.asList(arr), Color.ROYALBLUE));
		return transitions;
	}
}
