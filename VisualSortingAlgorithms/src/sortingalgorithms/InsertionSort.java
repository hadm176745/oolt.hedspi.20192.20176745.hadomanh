package sortingalgorithms;

import java.util.ArrayList;
import java.util.Arrays;

import element.Element;
import javafx.animation.ParallelTransition;
import javafx.animation.Transition;
import view.AnimationController;

public class InsertionSort extends AbstractSort {

	private ArrayList<Transition> transitions;

	public InsertionSort() {
		this.transitions = new ArrayList<>();
	}

	@Override
	public ArrayList<Transition> startSort(Element[] arr) {

		int n = AnimationController.NO_OF_ELEMENT;

		for (int i = 1; i < n; i++) {
			int j = i - 1;
			Element key = arr[i];

			ParallelTransition pt = new ParallelTransition();

			transitions.add(colorElement(arr, SELECT_COLOR, i));

			while (j >= 0 && arr[j].getValue() > key.getValue()) {
				pt.getChildren().add(arr[j].moveX(DX));
				arr[j + 1] = arr[j];
				j--;
			}

			arr[j + 1] = key;

			pt.getChildren().add(key.moveX(DX * (j + 1 - i)));
			transitions.add(pt);
			transitions.add(colorElement(arr, START_COLOR, j + 1));

		}

		transitions.add(colorElement(Arrays.asList(arr), SORTED_COLOR));

		return transitions;

	}

}
