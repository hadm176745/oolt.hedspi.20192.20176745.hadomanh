package sortingalgorithms;

import java.util.ArrayList;
import java.util.Arrays;

import element.Element;
import javafx.animation.Transition;
import view.AnimationController;

public class BubbleSort extends AbstractSort {

	private boolean swapped;
	private ArrayList<Transition> transitions;

	public BubbleSort() {
		this.transitions = new ArrayList<>();
	}

	private ArrayList<Transition> compareElement(Element[] arr, int a, int b) {
		ArrayList<Transition> transitions = new ArrayList<>();

		transitions.add(colorElement(arr, SELECT_COLOR, a, b));

		if (arr[a].getValue() > arr[b].getValue()) {
			transitions.add(swap(arr, a, b));
			swapped = true;
		}

		transitions.add(colorElement(arr, START_COLOR, a, b));

		return transitions;
	}

	private void bubbleSort(Element[] arr) {
		
		int n = AnimationController.NO_OF_ELEMENT;
		
		for (int i = 0; i < n; i++) {
			swapped = false;
			for (int j = 0; j < n - 1 - i; j++) {
				this.transitions.addAll(compareElement(arr, j, j + 1));
			}

			if (!swapped) {
				break;
			}
		}

	}

	@Override
	public ArrayList<Transition> startSort(Element[] arr) {
		bubbleSort(arr);

		this.transitions.add(colorElement(Arrays.asList(arr), SORTED_COLOR));

		return this.transitions;

	}

}
