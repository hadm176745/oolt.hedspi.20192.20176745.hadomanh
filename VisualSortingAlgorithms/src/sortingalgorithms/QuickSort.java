package sortingalgorithms;

import java.util.ArrayList;
import java.util.Arrays;

import element.Element;
import javafx.animation.Transition;
import javafx.scene.paint.Color;
import view.AnimationController;

public class QuickSort extends AbstractSort {

	private static final Color PIVOT_COLOR = Color.DARKMAGENTA;
	private ArrayList<Transition> transitions;

	public QuickSort() {
		this.transitions = new ArrayList<>();
	}

	private void quickSort(Element[] arr, int lo, int hi) {
		if (lo < hi) {
			int q = partition(arr, lo, hi);
			quickSort(arr, lo, q - 1);
			quickSort(arr, q + 1, hi);
		}
	}

	// last elt of array chosen as pivot
	private int partition(Element[] arr, int lo, int hi) {
		int i = lo;

		transitions.add(colorElement(arr, PIVOT_COLOR, hi));

		for (int j = lo; j < hi; j++) {
			transitions.add(colorElement(arr, SELECT_COLOR, j));
			if (arr[j].getValue() < arr[hi].getValue()) {
				transitions.add(swap(arr, i, j));
				transitions.add(colorElement(arr, START_COLOR, i));
				i++;
			} else {
				transitions.add(colorElement(arr, START_COLOR, j));
			}
		}
		transitions.add(swap(arr, i, hi));
		transitions.add(colorElement(arr, START_COLOR, i));

		return i;
	}

	@Override
	public ArrayList<Transition> startSort(Element[] arr) {

		int n = AnimationController.NO_OF_ELEMENT;

		quickSort(arr, 0, n - 1);
		transitions.add(colorElement(Arrays.asList(arr), SORTED_COLOR));

		return transitions;
	}
}
