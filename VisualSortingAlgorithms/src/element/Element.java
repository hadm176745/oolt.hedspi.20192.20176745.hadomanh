package element;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javafx.animation.TranslateTransition;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class Element extends Rectangle implements Comparable<Element> {

	private int value;

	public Element(int n) {
		this.value = n;
	}

	public int getValue() {
		return this.value;
	}

	private int getSpeed() {
		Properties p = new Properties();
		int result = 50;
		
		try {
			
			p.load(new FileInputStream("application.properties"));
			
			result = Integer.parseInt(p.getProperty("cnode.speed"));
			
		} catch (IOException e) {
			
			System.out.println(e.toString());
			
		}
		
		return result;

	}

	public TranslateTransition moveX(int x) {
		TranslateTransition t = new TranslateTransition();
		t.setNode(this);
		t.setDuration(Duration.millis(this.getSpeed()));
		t.setByX(x);

		return t;
	}

	@Override
	public int compareTo(Element o) {
		return this.getValue() - o.getValue();
	}

}
