package view;

import generator.ElementGenerator;
import sortingalgorithms.*;

import javafx.animation.SequentialTransition;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import element.Element;

public class AnimationController extends BorderPane {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 500;

	public static final int PADDING_RIGHT = 10;
	public static final int PADDING_BOTTOM = 100;

	public static final int NO_OF_ELEMENT = 40;

	private static AbstractSort abstractSort;

	private Pane display;
	private HBox buttonRow;

	private Button sortButton;
	private Button randomButton;
	private Button boostButton;
	private ChoiceBox<AbstractSort> choiceBox;

	private Element[] elements;

	public AnimationController() {
		this.display = new Pane();
		this.buttonRow = new HBox();

		this.setCenter(display);
		this.setBottom(buttonRow);

		this.sortButton = new Button("Sort");
		this.randomButton = new Button("Random");
		this.boostButton = new Button("Boost x 10");
		boostButton.setDisable(true);
		this.choiceBox = new ChoiceBox<>();

		this.elements = ElementGenerator.execute(NO_OF_ELEMENT);

		buttonRow.getChildren().add(sortButton);
		buttonRow.getChildren().add(randomButton);
		buttonRow.getChildren().add(boostButton);
		buttonRow.getChildren().add(choiceBox);

		buttonRow.setAlignment(Pos.CENTER);

		for (Node b : buttonRow.getChildren()) {
			HBox.setMargin(b, new Insets(5, 5, 20, 5));
		}

		List<AbstractSort> abstractSortList = new ArrayList<>();
		abstractSortList.add(new BubbleSort());
		abstractSortList.add(new SelectionSort());
		abstractSortList.add(new InsertionSort());
		abstractSortList.add(new MergeSort());
		abstractSortList.add(new QuickSort());
		abstractSortList.add(new HeapSort());
		abstractSortList.add(new BucketSort());

		display.getChildren().addAll(Arrays.asList(elements));

		sortButton.setOnAction(event -> {
			sortButton.setDisable(true);
			randomButton.setDisable(true);
			boostButton.setDisable(false);

			abstractSort = choiceBox.getSelectionModel().getSelectedItem();
			
			SequentialTransition sq = new SequentialTransition();

			sq.getChildren().addAll(abstractSort.startSort(elements));

			sq.setOnFinished(e -> {
				randomButton.setDisable(false);
				boostButton.setDisable(true);
			});
			
			sq.play();

			boostButton.setOnAction(e -> {
				sq.setRate(10);
				boostButton.setDisable(true);
			});

		});

		randomButton.setOnAction(event -> {
			sortButton.setDisable(false);
			boostButton.setDisable(true);
			display.getChildren().clear();

			elements = ElementGenerator.execute(NO_OF_ELEMENT);

			display.getChildren().addAll(Arrays.asList(elements));
		});

		choiceBox.setItems(FXCollections.observableArrayList(abstractSortList));

		choiceBox.getSelectionModel().select(6);

		choiceBox.setConverter(new StringConverter<AbstractSort>() {
			@Override
			public String toString(AbstractSort abstractSort) {
				if (abstractSort == null) {
					return "";
				} else {
					return abstractSort.getClass().getSimpleName();
				}
			}

			@Override
			public AbstractSort fromString(String s) {
				return null;
			}
		});

	}

}
