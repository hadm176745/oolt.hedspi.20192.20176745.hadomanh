package generator;

import view.AnimationController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import element.Element;
import javafx.scene.paint.Color;

public class ElementGenerator {

	public static Element[] execute(int n) {
		Element[] arr = new Element[n*2];
		Random r = new Random();
		
		List<Element> sorted = new ArrayList<Element>();

		for (int i = 0; i < n; i++) {
			arr[i] = new Element(1 + r.nextInt(n));
			arr[i].setX(i * (AnimationController.WINDOW_WIDTH / n));
			arr[i].setFill(Color.CRIMSON);
			build(arr[i], n);
			sorted.add(arr[i]);
		}
		
		Collections.sort(sorted);
		
		for (int i = n; i < 2*n; i++) {
			arr[i] = new Element(sorted.get(i-n).getValue());
			arr[i].setX(i * (AnimationController.WINDOW_WIDTH / n));
			arr[i].setFill(Color.CRIMSON);
			build(arr[i], n);
		}
		
		return arr;

	}

	private static void build(Element element, int n) {

		element.setY(
				(AnimationController.WINDOW_HEIGHT - AnimationController.PADDING_BOTTOM) * (n - element.getValue()) / n);

		element.setWidth(AnimationController.WINDOW_WIDTH / n - AnimationController.PADDING_RIGHT);

		element.setHeight(
				(AnimationController.WINDOW_HEIGHT - AnimationController.PADDING_BOTTOM) * element.getValue() / n);
	}
}
